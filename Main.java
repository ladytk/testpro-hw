class Main {
    public static void main(String[] args) {
        try {
            int[] arr1 = {1, 2, 3};
            System.out.println(arr1[3]);
        } catch (Exception e) {
            System.out.println("This is why QA Engineers always have to do boundary testing! The array only has 3 values and you've requested a 4th");
        }
    }
}